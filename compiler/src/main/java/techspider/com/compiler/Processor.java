package techspider.com.compiler;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import java.io.IOException;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

import techspider.com.autorecyclervieeannotations.AutoAdapter;

@SupportedAnnotationTypes("techspider.com.autorecyclervieeannotations.AutoAdapter")
public class Processor extends AbstractProcessor {

    private Filer filer;
    private Elements elements;
    private TypeName recycleViewAdapter = ClassName.get("android.support.v7.widget", "RecyclerView");
    private TypeName VIEW = ClassName.get("android.view", "View");
    private TypeName VIEW_GROUP = ClassName.get("android.view", "ViewGroup");
    private TypeName LAYOUT_INFLATER = ClassName.get("android.view", "LayoutInflater");

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
        elements = processingEnvironment.getElementUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {

        Set<? extends Element> lisOfElements = roundEnvironment.getElementsAnnotatedWith(AutoAdapter.class);
        for (Element element : lisOfElements) {
            try {
                System.out.println(element instanceof AutoAdapter);
                generateFile(element);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    private void generateFile(Element element) throws IOException {

        String currentPackageName = elements.getPackageOf(element).toString();
        TypeElement typeElement = (TypeElement) element.getEnclosingElement();
        String className = typeElement.getSimpleName() + "_Adapter";
        ClassName VIEW_HOLDER = ClassName.get(currentPackageName, className, "ViewHolder");

        AutoAdapter autoAdapter = element.getAnnotation(AutoAdapter.class);
        int layoutId = autoAdapter.value();

        MethodSpec onCreateViewHolder = MethodSpec.methodBuilder("onCreateViewHolder")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .addParameter(VIEW_GROUP.box(), "parent")
                .addParameter(int.class, "viewType")
                .addCode("$T itemView = $T.from(parent.getContext()).inflate($L, parent, false);", VIEW, LAYOUT_INFLATER, layoutId)
                .addCode("return new $T(itemView);", VIEW_HOLDER)
                .returns(VIEW_HOLDER)
                .build();

        MethodSpec onBindViewHolder = MethodSpec.methodBuilder("onBindViewHolder")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(Override.class)
                .addParameter(VIEW_HOLDER.box(), "holder")
                .addParameter(int.class, "position")
                .build();

        MethodSpec onItemCount = MethodSpec.methodBuilder("getItemCount")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(int.class)
                .addCode("return 0;")
                .build();

        MethodSpec viewHolderConstructor = MethodSpec.constructorBuilder()
                .addParameter(ParameterSpec.builder(VIEW, "itemView").build())
                .addCode("super(itemView);")
                .build();


        TypeSpec viewHolder = TypeSpec.classBuilder("ViewHolder")
                .addModifiers(Modifier.STATIC)
                .addMethod(viewHolderConstructor)
                .superclass(ClassName.get("android.support.v7.widget", "RecyclerView.ViewHolder"))
                .build();

        TypeSpec typeSpec = TypeSpec.classBuilder(className)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(onCreateViewHolder)
                .addMethod(onBindViewHolder)
                .addMethod(onItemCount)
                .superclass(ParameterizedTypeName.get(ClassName.get("android.support.v7.widget", "RecyclerView.Adapter"), TypeVariableName.get(className + ".ViewHolder")))
                .addType(viewHolder)
                .build();

        JavaFile file = JavaFile.builder(currentPackageName, typeSpec)
                .build();

        file.writeTo(filer);
    }
}
