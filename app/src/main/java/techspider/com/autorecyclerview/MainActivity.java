package techspider.com.autorecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import techspider.com.autorecyclervieeannotations.AutoAdapter;

public class MainActivity extends AppCompatActivity {

    @AutoAdapter(value = android.R.layout.simple_list_item_1, type = Student.class)
    RecyclerView rvTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
